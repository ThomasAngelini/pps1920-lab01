package lab01.tdd;

import java.util.stream.IntStream;

public class EvenStrategy implements SelectStrategy {

    public final CircularList circularList;

    public EvenStrategy(final CircularList circularList){
        this.circularList = circularList;
    }

    @Override
    public boolean apply(int element) {
        return this.circularList.isEmpty() ? false : this.doNext(element);
    }

    private boolean doNext(int element) {
        IntStream.range(0,element).forEach(e->this.circularList.next());
        return true;
    }
}
