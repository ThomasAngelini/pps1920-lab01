package lab01.tdd;

public final class SimpleStrategyFactory{

    public static SelectStrategy createEvenStrategy(CircularList circularList) {
        return new EvenStrategy(circularList);
    }

    public static SelectStrategy createMultipleStrategy(CircularList circularList) {
        return new MultipleOfStrategy(circularList);
    }

    public static SelectStrategy createEqualStrategy(CircularList circularList) {
        return new EqualsStrategy(circularList);
    }
}
