package lab01.tdd;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;

public class SimpleCircularList implements CircularList {

    private List<Integer> list;
    private int counter;
    private final int strategyNumber = 2;

    public  SimpleCircularList(){
        this.list = new ArrayList<>();
        this.counter = 0;
    }

    @Override
    public void add(int element) {
        this.list.add(element);
    }

    @Override
    public int size() {
        return this.list.size();
    }

    @Override
    public boolean isEmpty() {
        return this.list.isEmpty();
    }

    @Override
    public Optional<Integer> next() {
        Optional<Integer> returnValue = this.takeElementFromList();
        this.counter++;
        return returnValue;
    }

    @Override
    public Optional<Integer> previous() {
        Optional<Integer> returnValue = this.takeElementFromList();
        this.counter--;
        return returnValue;
    }

    @Override
    public void reset() {
        this.counter = 0;
    }

    @Override
    public Optional<Integer> next(SelectStrategy strategy) {
        return strategy.apply(this.strategyNumber) ? this.takeElementFromList() : Optional.empty();
    }

    private Optional<Integer> takeElementFromList(){
        return this.list.isEmpty() ? Optional.empty() : Optional.of(this.getElement());
    }

    private int getElement() {
        return this.list.get(Math.floorMod(this.counter, this.size()));
    }
}
