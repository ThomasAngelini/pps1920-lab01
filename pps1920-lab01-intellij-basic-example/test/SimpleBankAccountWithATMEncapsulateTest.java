import lab01.example.model.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SimpleBankAccountWithATMEncapsulateTest{

    private AccountHolder accountHolder;
    private BankAccount bankAccount;
    private ATMStrategy atmStrategy;
    private final double ATMFee = 1;
    private ATMStrategyEncapsulate strategyEncapsulate;

    @BeforeEach
    void beforeEach(){
        accountHolder = new AccountHolder("Mario", "Rossi", 1);
        this.atmStrategy = (balance) -> balance - this.ATMFee;
        this.strategyEncapsulate = new SimpleATMStrategyEncapsulate(this.atmStrategy);
        bankAccount = new SimpleBankAccountWithATMEncapsulate(accountHolder, 0, this.strategyEncapsulate);
    }

    @Test
    void testDeposit() {
        this.bankAccount.deposit(1,30);
        assertEquals(29, bankAccount.getBalance());
    }

    @Test
    void testErroneusDeposit(){
        this.bankAccount.deposit(1,0.5);
        assertEquals(0,bankAccount.getBalance());
    }

    @Test
    void testWithdraw(){
        this.bankAccount.deposit(1,30);
        this.bankAccount.withdraw(1,10);
        assertEquals(18,bankAccount.getBalance());
    }

    @Test
    void testErroneusWithdraw(){
        this.bankAccount.deposit(1,30);
        this.bankAccount.withdraw(1,30);
        assertEquals(29,bankAccount.getBalance());
    }

}
