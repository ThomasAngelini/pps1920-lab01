package lab01.example.model;

public class SimpleATMStrategyEncapsulate implements ATMStrategyEncapsulate{

    private final ATMStrategy atmStrategy;

    public SimpleATMStrategyEncapsulate(ATMStrategy atmStrategy){
        this.atmStrategy=atmStrategy;
    }


    @Override
    public double withdraw(double balance, double amount) {
        if (this.isWithdrawAllowed(balance,amount)){
            balance = this.updateBalanceWithATM(balance-amount);
        }
        return balance;
    }

    @Override
    public double deposit(double balance, double amount) {
        if (this.isDepositAllowed(balance,amount)){
            balance = this.updateBalanceWithATM(balance+amount);
        }
        return balance;
    }

    private boolean isWithdrawAllowed(final double balance, final double amount){
        return this.atmStrategy.applyATMStrategy(balance) >= amount;
    }

    private boolean isDepositAllowed(final double balance, final double amount){
        return (amount + this.atmStrategy.applyATMStrategy(balance) >= 0);
    }

    private double updateBalanceWithATM(final double balance){
        return this.atmStrategy.applyATMStrategy(balance);
    }

}
