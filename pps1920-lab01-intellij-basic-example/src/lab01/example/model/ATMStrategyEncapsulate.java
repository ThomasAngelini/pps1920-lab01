package lab01.example.model;

public interface ATMStrategyEncapsulate {
    public double withdraw(double balance, double amount);
    public double deposit(double balance, double amount);
}
