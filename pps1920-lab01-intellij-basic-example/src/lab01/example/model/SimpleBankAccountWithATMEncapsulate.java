package lab01.example.model;

public class SimpleBankAccountWithATMEncapsulate implements BankAccount {

    private double balance;
    private final AccountHolder holder;
    private final ATMStrategyEncapsulate atmStrategy;

    public SimpleBankAccountWithATMEncapsulate(final AccountHolder holder, final double balance, ATMStrategyEncapsulate atmStrategy) {
        this.holder = holder;
        this.balance = balance;
        this.atmStrategy = atmStrategy;
    }

    @Override
    public AccountHolder getHolder() {
        return this.holder;
    }

    @Override
    public double getBalance() {
        return this.balance;
    }

    @Override
    public void deposit(int usrID, double amount) {
        if (checkUser(usrID)){
            this.balance = this.atmStrategy.deposit(this.balance,amount);
        }
    }

    @Override
    public void withdraw(int usrID, double amount) {
        if (checkUser(usrID)){
            this.balance = this.atmStrategy.withdraw(this.balance,amount);
        }
    }

    private boolean checkUser(final int id) {
        return this.holder.getId() == id;
    }

}
